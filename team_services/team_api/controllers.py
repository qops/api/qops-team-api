########################################################################
# Copyright 2018 Jason Alan Smith
#
# This file is part of Quality Operations Center.
#
# Quality Operations Center is free software: you can redistribute it
# and/or modify it under the terms of the GNU General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# Quality Operations Center is distributed in the hope that it will be
# useful, but WITHOUT ANY WARRANTY; without even the implied warranty
# of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Quality Operations Center.
# If not, see <https://www.gnu.org/licenses/>.
########################################################################


# Import flask dependencies
from flask import Blueprint, request, render_template, \
    flash, g, session, redirect, url_for

import logging

# Import models
from .models import Team
from .models import TeamMember
from .models import TeamRole

from .forms import TeamProfileForm

# Import the database object from the main app module
from qops_desktop import db

# Define the blueprint: 'auth', set its url prefix: app.url/auth
mod_team = Blueprint('team', __name__, url_prefix='/team')


@mod_team.context_processor
def store():
    store_dict = {'serviceName': 'Team',
                  'serviceDashboardUrl': url_for('team.dashboard'),
                  'serviceBrowseUrl': url_for('team.browse'),
                  'serviceNewUrl': url_for('team.new'),
                  }
    return store_dict

# Set the route and accepted methods


@mod_team.route('/', methods=['GET'])
def team():
    return render_template('team/team_dashboard.html')


@mod_team.route('/dashboard', methods=['GET'])
def dashboard():
    return render_template('team/team_dashboard.html')


@mod_team.route('/browse', methods=['GET'])
def browse():
    teams = Team.query.with_entities(Team.id, Team.identifier, Team.name, Team.description, TeamRole.name.label(
        'role_id')).join(TeamRole, Team.role_id == TeamRole.id).all()
    return render_template('team/team_browse.html', teams=teams)


@mod_team.route('/new', methods=['GET', 'POST'])
def new():
    team = Team()
    form = TeamProfileForm(request.form)
    form.role_id.choices = [(r.id, r.name) for r in TeamRole.query.all()]
    if request.method == 'POST':
        logger = logging.getLogger('qops')
        logger.debug('IN TEAM.NEW(POST)')
        logger.debug('Team Identifier : {0}'.format(form.identifier.data))
        logger.debug('Team Name       : {0}'.format(form.name.data))
        logger.debug('Team Description: {0}'.format(form.description.data))
        logger.debug('Team Role       : {0}'.format(form.role_id.data))
        form.populate_obj(team)
        team.identifier = Team.get_next_identifier()
        logger.debug(team.role_id)
        db.session.add(team)
        db.session.commit()
        return redirect(url_for('team.browse'))
    return render_template('team/team_new.html', team=team, form=form)


@mod_team.route('/profile', methods=['GET', 'POST'])
@mod_team.route('/profile/<int:team_id>', methods=['GET', 'POST'])
def profile(team_id):
    logger = logging.getLogger('qops')
    team = Team.query.get(team_id)
    form = TeamProfileForm(obj=team)
    form.role_id.choices = [(r.id, r.name) for r in TeamRole.query.all()]
    if request.method == 'POST':
        form = TeamProfileForm(request.form)
        form.populate_obj(team)
        db.session.add(team)
        db.session.commit()
        return redirect(url_for('team.browse'))
    return render_template('team/team_profile.html', team=team, form=form)


@mod_team.route('/view', methods=['GET', 'POST'])
@mod_team.route('/view/<int:team_id>', methods=['GET', 'POST'])
def team_view(team_id=None):
    #team = Team.query.get(team_id)
    form = TeamProfileForm(obj=team)
    form.role_id.choices = [(r.id, r.name) for r in TeamRole.query.all()]
    if request.method == 'POST':
        form = TeamProfileForm(request.form)
        form.populate_obj(team)
        db.session.add(team)
        db.session.commit()
        return redirect(url_for('team.browse'))
    return render_template('team/team_view.html', team=team, form=form)


@mod_team.route('/profile/dashboard', methods=['GET'])
@mod_team.route('/profile/<int:team_id>/dashboard', methods=['GET'])
def team_dashboard(team_id=None):
    if team_id:
        team = Team.query.get(team_id)
    else:
        team = None
    return render_template('team/team_single_dashboard.html', team=team)


@mod_team.route('/profile/members', methods=['GET'])
@mod_team.route('/profile/<int:team_id>/members', methods=['GET'])
def team_members(team_id=None):
    if team_id:
        team = Team.query.get(team_id)
    else:
        team = None
    return render_template('team/team_single_members.html', team=team)


@mod_team.route('/profile/tasks', methods=['GET', 'POST'])
@mod_team.route('/profile/<int:team_id>/tasks', methods=['GET'])
def team_tasks(team_id=None):
    if team_id:
        team = Team.query.get(team_id)
    else:
        team = None
    return render_template('team/team_single_tasks.html', team=team)


@mod_team.route('/profile/backlog', methods=['GET', 'POST'])
@mod_team.route('/profile/<int:team_id>/backlog', methods=['GET'])
def team_backlog(team_id=None):
    if team_id:
        team = Team.query.get(team_id)
    else:
        team = None
    return render_template('team/team_single_backlog.html', team=team)


@mod_team.route('/profile/communication', methods=['GET', 'POST'])
@mod_team.route('/profile/<int:team_id>/communication', methods=['GET'])
def team_communication(team_id=None):
    if team_id:
        team = Team.query.get(team_id)
    else:
        team = None
    return render_template('team/team_single_communication.html', team=team)
