# Quality Operations Center

# Mission



# Vision



# Services

<table>
  <tr>
    <td>Home</td>
    <td>Customer</td>
    <td>Product</td>
    <td>Project</td>
    <td>Issue</td>
    <td>Backlog</td>
    <td>User Story</td>
    <td>Requirement</td>
  </tr>
  <tr>
    <td>Test Case</td>
    <td>Iteration</td>
    <td>Release</td>
    <td>Build</td>
    <td>Task</td>
    <td>Tag</td>
    <td>Document</td>
    <td>Person</td>
  </tr>
  <tr>
    <td>Team</td>
    <td>Communication</td>
    <td>Location</td>
    <td>Configuration</td>
    <td>Sales</td>
    <td>Marketing</td>
    <td>System</td>
    <td>Dashboard</td>
  </tr>
  <tr>
    <td>Implementation</td>
    <td>Deployment</td>
    <td>Customer Care</td>
  </tr>
</table>

# System Operational Modes

The Quality Operations Center platform can operate in one of four modes.  Each operational mode consists of terminology, user interface elements, and database schema unique to that mode.

* Individual
* Community
* Not-For-Profit Organizational
* For-Profit Organizational

## Individual

The Individual operational mode supports individual free/libre and open source software developers.  These individual software developers alone produce their software.  From enhancements, bug fixes, refactoring, documentation, release management to user support, these individual software developers do it all to produce their software.  The only interaction with the user community is in receiving software patches contributed by others and with supporting the software's end-users.

## Community



## Not-For-Profit Organizational



## For-Profit Organizational



# Customers

* Individual
* For-Profit Organization
* Not-For-Profit Organization

## An Individual



## A For-Profit Organization



## A Not-For-Profit Organization



# Databases

* QOps
* QOpsLog
* QOpsArchive
* QOpsDW
* QOpsAudit

* QOpsQA
* QOpsDev
* QOpsCC # What is CC?

## QOps



## QOpsLog



## QOpsArchive



## QOpsDW



## QOpsAudit



# System User Roles 

* Quality Assurance
* Development
* Customer Care
* Implementation / Deployment
* Business / Process / Requirements Analysis

## Quality Assurance



## Development



## Support / Engagement



## Implementation / Deployment



## Business / Process / Requirements Analysis



# System Rules / Assumptions

## Assumptions

* 

## System Rules

* 

# Application Services

## Home Service



## Customer Service



### Data Dictionary



## Product Service



### Data Dictionary



## Project Service



### Data Dictionary



## Team Service



### Data Dictionary



## Task Service



### Data Dictionary



## Person Service



### Data Dictionary



## Requirement Service



### Data Dictionary



## Release Service



### Data Dictionary



## Iteration Service



### Data Dictionary



## Backlog Service



### Data Dictionary



## Sales Service



### Data Dictionary



## Marketing Service



### Data Dictionary



## Document Service



### Data Dictionary



## Issue Service



### Data Dictionary



## Tag Service



### Data Dictionary



## Dashboard Service



## System Service



### Data Dictionary



## Build Service



### Data Dictionary

